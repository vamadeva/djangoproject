from django.shortcuts import render, redirect
from mainapp.forms import ContactForm
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from mainapp.models import Quote,Service,Slider,Testimonial,TeamMember
from django.core.exceptions import ValidationError


def home(request):
    services = Service.objects.all()
    sliders  = Slider.objects.all()
    testimonials = Testimonial.objects.all()
    team_members = TeamMember.objects.all()
    return render(request,'index.html',{'services':services,'sliders':sliders,'testimonials':testimonials,'team_members':team_members})

def aboutUs(request):
    
    return render(request,'about.html')

def contact(request):
    
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            
            subject  = form.cleaned_data["subject"]
            message  = form.cleaned_data["message"]
            name     = form.cleaned_data["name"]
            email    = form.cleaned_data["email"]
            phone    = form.cleaned_data["phone"]
            
            htmly     = get_template('contact.email.html')
            email_data = { 'name': name,'email':email ,'subject':subject,'message':message,'phone':phone}
            html_content = htmly.render(email_data)
            
          
            from_email = 'manojsingh783@gmail.com'
            to_email = 'manojsingh783@gmail.com'
            text_content = "This is an important message."
         
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to_email])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            
            form.save()
            form = ContactForm()
            redirect('home')
           
        
    else:
        form = ContactForm()    
        
    return render(request,'contact.html',{'form':form})

def myQuote(request):
    
    if request.method == 'POST':

        name = request.POST.get("name")
        email = request.POST.get("email")
        service = request.POST.get("service")
        message = request.POST.get("message")
        
        quote = Quote()
        quote.name = name
        quote.email = email
        quote.service = service
        quote.message = message
        
        try:
            quote.full_clean()
            quote.save()
            return redirect('quote')
        except ValidationError:
            pass
        
    else:
        return render(request,'quote.html')


