from django.db import models
from tinymce.models import HTMLField

# Create your models here.
class Contact(models.Model):
    
    name    = models.CharField(max_length=100)
    email   = models.CharField(max_length=100)
    phone   = models.CharField(max_length=15)
    subject = models.CharField(max_length=100)
    message = models.TextField()
    
class Quote(models.Model):
    services = [
        ("Web Development", "Web Development"),
        ("Apps Development", "Apps Development"),
        ("SEO Optimization", "SEO Optimization"),
    ]
    name    = models.CharField(max_length=100)
    email   = models.CharField(max_length=100)
    service = models.CharField(max_length=100,choices=services)
    message = models.TextField()
    
class Service(models.Model):
    title    = models.CharField(max_length=100)
    short_description = HTMLField()
    description = HTMLField()
    service_image = models.ImageField(upload_to='service_images')
    slug = models.SlugField(default="", null=False)
    
class Slider(models.Model):
    title    = models.CharField(max_length=100)
    description = HTMLField()
    slider_image = models.ImageField(upload_to='slider_images')
    
class Testimonial(models.Model):
    client_name    = models.CharField(max_length=100)
    client_designation = models.CharField(max_length=100)
    client_description = HTMLField()
    client_image = models.ImageField(upload_to='client_images')
    
class TeamMember(models.Model):
    full_name    = models.CharField(max_length=100)
    designation  = models.CharField(max_length=100)
    member_image = models.ImageField(upload_to='team_member_images')
    