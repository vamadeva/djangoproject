from django.forms import ModelForm
from mainapp.models import Contact, Service

class ContactForm(ModelForm):
    
    def __init__(self, *args, **kwargs):
            super(ContactForm, self).__init__(*args, **kwargs)
            self.fields['name'].widget.attrs = {
                'class': 'form-control border-0 bg-light px-4',
                'placeholder':'Your Name'
                }
            self.fields['email'].widget.attrs = {
                'class': 'form-control border-0 bg-light px-4',
                'placeholder':'Your Email'
                }
            self.fields['phone'].widget.attrs = {
                'class': 'form-control border-0 bg-light px-4',
                'placeholder':'Your Phone'
                }
            self.fields['subject'].widget.attrs = {
                'class': 'form-control border-0 bg-light px-4 py-3',
                'placeholder':'Your Subject'
                }
            self.fields['message'].widget.attrs = {
                'class': 'form-control border-0 bg-light px-4 py-3',
                'placeholder':'Your Message',
                'rows':4
                }
    
    class Meta:
        model  = Contact
        fields = ['name','email','phone','subject','message']
        
# class ServiceForm(ModelForm):
    
#     def __init__(self, *args, **kwargs):
#         super(ContactForm, self).__init__(*args, **kwargs)
#         self.fields['title'].widget.attrs = {
#             'class': 'form-control border-0 bg-light px-4',
#             'placeholder':'Your Name'
#             }
#         self.fields['short_description'].widget.attrs = {
#             'class': 'form-control border-0 bg-light px-4',
#             'placeholder':'Your Email'
#             }
#         self.fields['phone'].widget.attrs = {
#             'class': 'form-control border-0 bg-light px-4',
#             'placeholder':'Your Phone'
#             }
#         self.fields['subject'].widget.attrs = {
#             'class': 'form-control border-0 bg-light px-4 py-3',
#             'placeholder':'Your Subject'
#             }
#         self.fields['message'].widget.attrs = {
#             'class': 'form-control border-0 bg-light px-4 py-3',
#             'placeholder':'Your Message',
#             'rows':4
#             }
    
#     class Meta:
#         model=Service
#         fields = ['title','short_description','description','service_image']

       