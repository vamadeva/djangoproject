
from django.urls import path, include
from mainapp import views

urlpatterns = [
    path('',views.home,name='home'),
    path('about-us',views.aboutUs,name='about'),
    path('contact',views.contact,name='contact'),
    path('myquote',views.myQuote,name='quote'),
]
