# Generated by Django 4.2.7 on 2023-11-16 05:44

from django.db import migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0003_service_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='description',
            field=tinymce.models.HTMLField(),
        ),
    ]
