from django.contrib import admin
from mainapp.models import Contact,Service,Quote,Slider,Testimonial,TeamMember

# Register your models here.
class ContactAdmin(admin.ModelAdmin):
    list_display  = ['name','email','phone']
    list_filter   = ["name", "email","phone"]
    search_fields = ["email","phone"]
    
class ServiceAdmin(admin.ModelAdmin):
    list_display  = ['title','short_description']
    list_filter   = ["title", "short_description"]
    search_fields = ["title","short_description"]
    prepopulated_fields = {'slug':('title',)}
    
class QuoteAdmin(admin.ModelAdmin):
    list_display  = ['name','email','service']
    list_filter   = ["name", "email","service"]
    search_fields = ["email","service"]
    
class SliderAdmin(admin.ModelAdmin):
    list_display  = ['title','description']
    
class TestimonialAdmin(admin.ModelAdmin):
    list_display = ['client_name','client_designation']
    
class TeamMemberAdmin(admin.ModelAdmin):
    list_display = ['full_name','designation']

admin.site.register(Contact,ContactAdmin)
admin.site.register(Service,ServiceAdmin)
admin.site.register(Quote,QuoteAdmin)
admin.site.register(Slider,SliderAdmin)
admin.site.register(Testimonial,TestimonialAdmin)
admin.site.register(TeamMember,TeamMemberAdmin)
